package com.xa.medic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.Menu_Role;

@Repository
public interface Menu_RoleRepo extends JpaRepository<Menu_Role, Long>{

}

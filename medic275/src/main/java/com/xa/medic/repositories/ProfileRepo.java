package com.xa.medic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.Profile;

@Repository
public interface ProfileRepo extends JpaRepository<Profile, Long> {

}

package com.xa.medic.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long>{

	@Query(value="SELECT * FROM users u WHERE u.Email = ?1 AND u.Password = ?2", nativeQuery = true) 
	//?1 = Parameter 1 ke email, ?2 = Parameter 2 ke password
	// users = nama tabel
	// u.Email = nama object yg ada di model (bukan nama kolom)
	List<User> getLogin(String Email, String Password);
	
	@Query(value="SELECT * FROM users u WHERE u.email LIKE %?% OR u.password LIKE %?%",
			nativeQuery = true)
	List<User> cariuser(String email, String password); //email dan lastlogin ada di index userapi
	
	@Query(value="SELECT biodata.fullname, users.email, biodata.mobile_phone "
			+ "from biodata join users on biodata.id = users.biodata_id;",
			nativeQuery = true)
	List<Map<String, Object>> getDataUser();
	// Fungsi Map dipakai ketika menggunakan lebih dari 1 models (model biodata dan model user)
	// Kemudian model tersebut dijadikan string semua lalu jadi object
	
	@Query(value="SELECT b.fullname, u.email, b.mobile_phone "
			+ "from biodata b join users u on b.id=u.biodata_id "
			+ "WHERE b.fullname LIKE %?1% AND u.email LIKE %?2% "
			+ "AND b.mobile_phone LIKE %?3%",
			nativeQuery = true)
	List<Map<String, Object>> cariuser(String fullname, String email, String mobile_phone);
}

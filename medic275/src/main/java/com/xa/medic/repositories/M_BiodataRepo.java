package com.xa.medic.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.M_Biodata;

@Repository
public interface M_BiodataRepo extends JpaRepository<M_Biodata, Long> {
	
}

package com.xa.medic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.M_Biodata_Address;

@Repository
public interface M_Biodata_AddressRepo extends JpaRepository<M_Biodata_Address, Long> {
	
}

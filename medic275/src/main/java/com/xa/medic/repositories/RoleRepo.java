package com.xa.medic.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.Role;

@Repository
public interface RoleRepo extends JpaRepository<Role, Long>{

}

package com.xa.medic.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.medic.models.Menu;

@Repository
public interface MenuRepo extends JpaRepository<Menu, Long>{

	@Query(value="SELECT * FROM menu m WHERE parent_id IS NULL",
			nativeQuery=true)
	List<Menu> getParentMenu();
	
	@Query(value="SELECT * FROM menu m WHERE parent_id = ?1",
			nativeQuery=true)
	List<Menu> getSubMenu(Long parent_id);
}

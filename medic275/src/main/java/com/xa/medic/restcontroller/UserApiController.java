package com.xa.medic.restcontroller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.medic.models.User;
import com.xa.medic.repositories.M_BiodataRepo;
import com.xa.medic.repositories.UserRepo;

@RestController
@RequestMapping(value="/api/user")
@CrossOrigin("*") //Mengatur port mana saja yg bisa masuk ke aplikasi, "*" = semuanya bisa
public class UserApiController {
	
	@Autowired
	private UserRepo userrepo;
	
	@Autowired
	private M_BiodataRepo m_biodatarepo;
	
	@GetMapping("/")
	public ResponseEntity<List<User>> showuser() {
		try {
			List<User> user = this.userrepo.findAll();
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}
	
	@GetMapping(value="/{id}")
	public ResponseEntity<Object> getUserEntity(@PathVariable("id") Long id) {
		Optional<User> user = this.userrepo.findById(id);
		if(user.isPresent()) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("No Data", HttpStatus.OK);
		}
	}
	
	@PostMapping(value="/")
	public ResponseEntity<Object> saveUser(@RequestBody User user) { //RequestBody ambil di body html kemudian disave
		User user_ = this.userrepo.save(user);
		if(user_.equals(user)) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Update Data Gagal", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping(value="/{id}")
	public ResponseEntity<Object> updateUser(@RequestBody User user, @PathVariable("id") Long id) {
		Optional<User> user_ = this.userrepo.findById(id);
		if(user_.isPresent()) {
			user.setId(id);
			this.userrepo.save(user);
			return new ResponseEntity<>(user_, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(user_, HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value="/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id) {
		this.userrepo.deleteById(id);
		return new ResponseEntity<>("Hapus User Berhasil", HttpStatus.OK);
	}
	
	@GetMapping(value="/cari/{email}/{password}")
	public ResponseEntity<List<User>> cariuser(
			@PathVariable("email") String email,
			@PathVariable("password") String password
			) {
		try {
			List<User> user = this.userrepo.cariuser(email, password);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping(value="getPhone")
	public ResponseEntity<List<Map<String, Object>>> getPhone() {
		try {
			List<Map<String, Object>> getphone = this.userrepo.getDataUser();
			return new ResponseEntity<>(getphone, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping(value="/cari/{fullname}/{email}/{mobile_phone}")
	public ResponseEntity<List<Map<String, Object>>> cariuser(
			@PathVariable("fullname") String fullname,
			@PathVariable("email") String email,
			@PathVariable("mobile_phone") String mobile_phone
			) {
		try {
			List<Map<String, Object>> cariuser = this.userrepo.cariuser(fullname.toLowerCase(), email.toLowerCase(), mobile_phone.toLowerCase());
			return new ResponseEntity<>(cariuser, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}

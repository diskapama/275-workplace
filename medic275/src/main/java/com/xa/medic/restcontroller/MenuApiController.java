package com.xa.medic.restcontroller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xa.medic.models.Menu;
import com.xa.medic.repositories.MenuRepo;

@RestController
@RequestMapping(value="/api/menu")
@CrossOrigin("*")
public class MenuApiController {
	
	@Autowired
	private MenuRepo menurepo;
	
	@GetMapping("/")
	public ResponseEntity<List<Menu>> showmenu() {
		try {
			List<Menu> menu = this.menurepo.findAll();
			return new ResponseEntity<>(menu, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/getParentMenu") // getParentMenu dari MenuRepo
	public ResponseEntity<List<Menu>> getParentMenu() {
		try {
			List<Menu> menu = this.menurepo.getParentMenu();
			return new ResponseEntity<>(menu, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/getSubMenu/{parent_id}")
	public ResponseEntity<List<Menu>> getSubMenu(@PathVariable("parent_id") Long pid) {
		try {
			List<Menu> menu = this.menurepo.getSubMenu(pid);
			return new ResponseEntity<>(menu, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}

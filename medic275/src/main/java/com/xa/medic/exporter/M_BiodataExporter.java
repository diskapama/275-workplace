package com.xa.medic.exporter;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.xa.medic.models.M_Biodata;

public class M_BiodataExporter {

	private List<M_Biodata> listM_Biodata;
	
	public M_BiodataExporter(List<M_Biodata> listm_biodata) {
		this.listM_Biodata = listm_biodata;
	}
	
	private void writeTableHeaderM_Biodata(PdfPTable table) {
		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.GRAY);
		cell.setPadding(5);
		
		Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
		font.setColor(Color.WHITE);
		
		cell.setPhrase(new Phrase("Fullname", font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Mobile Phone", font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("Image", font));
		table.addCell(cell);
	}
	
	private void writeTableData(PdfPTable table) {
		for(M_Biodata m_biodata:listM_Biodata) {
			table.addCell(String.valueOf(m_biodata.getFullname()));
			table.addCell(String.valueOf(m_biodata.getMobile_Phone()));
			table.addCell(String.valueOf(m_biodata.getImage()));
		}
	}
	
	public void export(HttpServletResponse response) throws DocumentException, IOException {
		Document document = new Document(PageSize.A4);
		PdfWriter.getInstance(document, response.getOutputStream());
		
		document.open();
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setSize(18);
		font.setColor(Color.BLACK);
		
		Paragraph p = new Paragraph("List of Pofile", font);
		p.setAlignment(Paragraph.ALIGN_CENTER);
		
		document.add(p);
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setWidths(new float[] {3.0f, 3.0f, 30f});
		table.setSpacingBefore(10);
		
		writeTableHeaderM_Biodata(table);
		writeTableData(table);
		
		document.add(table);
		document.close();
	}
}

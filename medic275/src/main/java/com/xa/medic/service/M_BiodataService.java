package com.xa.medic.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xa.medic.models.M_Biodata;
import com.xa.medic.repositories.M_BiodataRepo;

@Service
@Transactional
public class M_BiodataService {

	@Autowired M_BiodataRepo m_biodatarepo;
	
	public List<M_Biodata> listAll() {
		return this.m_biodatarepo.findAll();
	}
}

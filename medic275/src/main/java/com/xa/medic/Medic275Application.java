package com.xa.medic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Medic275Application {

	public static void main(String[] args) {
		SpringApplication.run(Medic275Application.class, args);
	}

}

package com.xa.medic.controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.medic.models.M_Biodata;
import com.xa.medic.models.Role;
import com.xa.medic.models.User;
import com.xa.medic.repositories.M_BiodataRepo;
import com.xa.medic.repositories.RoleRepo;
import com.xa.medic.repositories.UserRepo;

@Controller
@RequestMapping(value="/user/")
public class UserController {
	@Autowired
	private UserRepo userrepo;
	
	@Autowired
	private M_BiodataRepo m_biodatarepo;
	
	@Autowired
	private RoleRepo rolerepo;
	
	@Autowired private JavaMailSender javamailsender;
	
	private static String bytesToHex(byte[] hash) {
		StringBuilder hexString = new StringBuilder(2 * hash.length);
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}
	
	@PostMapping(value="ceklogin")
	ModelAndView ceklogin(@ModelAttribute User users, BindingResult result, HttpSession sess) { //User = model, users = nama object
		String redirect = "";
		if (!result.hasErrors()) {
			String email = (String) result.getFieldValue("Email");
			String password = (String) result.getFieldValue("Password");
			try {
				MessageDigest digest;
				digest = MessageDigest.getInstance("SHA-256");
				byte[] encodehash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
				String pass = bytesToHex(encodehash); // Password di konfersi ke byte code hash
				
				List<User> getuser = this.userrepo.getLogin(email, pass); // Membandingkan email dan password
				
				try {
					sess.setAttribute("uid", getuser.get(0).getId());
					sess.setAttribute("email", getuser.get(0).getEmail());
					System.out.println("Access Granted!");
					redirect = "redirect:/home";
				} catch (Exception e) {
					System.out.println("Access Denied!");
					redirect = "redirect:/";
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				redirect = "redirect:/";
			}
		}
		return new ModelAndView(redirect);
	}
	
	@GetMapping(value="index")
	public ModelAndView user() { //user nama method. gk harus sama nama value
		ModelAndView view = new ModelAndView("/user/index");
		List<User> userdata = this.userrepo.findAll();
		view.addObject("udata", userdata); //userdata adalah variable
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/user/form");
		User user = new User();
		view.addObject("userform", user);
		List<M_Biodata> biodata = this.m_biodatarepo.findAll(); //List array 
		view.addObject("combobiodata", biodata);
		List<Role> role = this.rolerepo.findAll();
		view.addObject("comborole", role);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute User user, BindingResult result) throws Exception { // BindingResult mengambil file form xml/ membandingkan model dengan database
		if(!result.hasErrors()) {
			//LocalDate date = LocalDate.now();
			//user.setLast_Login(date);
			
			String pass = (String) result.getFieldValue("Password");
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] encodehash = digest.digest(pass.getBytes(StandardCharsets.UTF_8));
			user.setPassword(bytesToHex(encodehash));
			this.userrepo.save(user);
			
			String email = (String) result.getFieldValue("Email");
			String subject = "New User Created";
			String message = "User With Email : "+email+" Created";
			
			sendmail(email, subject, message);
		}
		return new ModelAndView("redirect:/user/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/user/form");
		User user = this.userrepo.findById(id).orElse(null);
		view.addObject("userform", user);
		List<M_Biodata> biodata = this.m_biodatarepo.findAll();
		view.addObject("combobiodata", biodata);
		List<Role> role = this.rolerepo.findAll();
		view.addObject("comborole", role);
		return view;
	}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView deluser(@PathVariable("id") Long id) {
		if(id != null) {
			this.userrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/user/index");
	}
	
	public void sendmail(String address, String subject, String message) throws Exception {
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(address);
		msg.setSubject(subject);
		msg.setTo(message);
		
		javamailsender.send(msg);
	}
}

package com.xa.medic.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.medic.models.M_Biodata;
import com.xa.medic.models.M_Biodata_Address;
import com.xa.medic.repositories.M_BiodataRepo;
import com.xa.medic.repositories.M_Biodata_AddressRepo;

@Controller
@RequestMapping(value="/m_biodata_address/")
public class M_Biodata_AddressController {
	
	@Autowired
	private M_Biodata_AddressRepo m_biodata_addressrepo;
	
	@Autowired
	private M_BiodataRepo m_biodatarepo;
	
	@GetMapping(value="index")
	public ModelAndView m_biodata_address() {
		ModelAndView view = new ModelAndView("/m_biodata_address/index");
		List<M_Biodata_Address> m_biodata_addressData = this.m_biodata_addressrepo.findAll();
		view.addObject("mbadata", m_biodata_addressData);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/m_biodata_address/form");
		M_Biodata_Address m_biodata_address = new M_Biodata_Address();
		view.addObject("addressform", m_biodata_address);
		List<M_Biodata> m_biodataSelect = this.m_biodatarepo.findAll();
		view.addObject("combobiodata", m_biodataSelect);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute M_Biodata_Address m_biodata_address, BindingResult result) { // BindingResult mengambil file form xml/ membandingkan model dengan database
		if(!result.hasErrors()) {
			this.m_biodata_addressrepo.save(m_biodata_address);
		}
		return new ModelAndView("redirect:/m_biodata_address/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/m_biodata_address/form");
		M_Biodata_Address m_biodata_address = this.m_biodata_addressrepo.findById(id).orElse(null);
		view.addObject("addressform", m_biodata_address);
		List<M_Biodata> m_biodataSelect = this.m_biodatarepo.findAll();
		view.addObject("combobiodata", m_biodataSelect);
		return view;
	}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView delbiodata(@PathVariable("id") Long id) {
		if(id != null) {
			this.m_biodata_addressrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/m_biodata_address/index");
	}
}

package com.xa.medic.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.medic.models.Profile;
import com.xa.medic.repositories.ProfileRepo;

@Controller
@RequestMapping(value="/profile/") //GetMapping untuk request hal awal
public class ProfileController {
	
	@Autowired
	private ProfileRepo profilrepo;

	@GetMapping(value="index")
	public ModelAndView profile() {
		ModelAndView view = new ModelAndView("/profile/index");
		List<Profile> profiledata = this.profilrepo.findAll();
		view.addObject("pdata", profiledata);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/profile/form");
		Profile profile = new Profile();
		view.addObject("profileform", profile);
		return view;
	}

	@PostMapping(value="save") // PostMapping untuk submit data ke database
	public ModelAndView save(@ModelAttribute Profile profile, BindingResult result) {
		if(!result.hasErrors()) {
			this.profilrepo.save(profile);
		}
		return new ModelAndView("redirect:/profile/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/profile/form");
		Profile profile = this.profilrepo.findById(id).orElse(null);
		view.addObject("profileform", profile);
		return view;
	}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView delprofile(@PathVariable("id") Long id) {
		if(id != null) {
			this.profilrepo.deleteById(id);
		}
		return new ModelAndView("redirect:/profile/index");
	}
}

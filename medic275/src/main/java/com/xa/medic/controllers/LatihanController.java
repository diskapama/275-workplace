package com.xa.medic.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/latihan/")
public class LatihanController {

	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/latihan/index");
		return view;
	}
	
	@GetMapping(value="book")
	public ModelAndView book() {
		ModelAndView view = new ModelAndView("/latihan/book");
		return view;
	}
	
	//@GetMapping(value="book/{judul}/{pengarang}")
	//public ModelAndView book(@para) {
}

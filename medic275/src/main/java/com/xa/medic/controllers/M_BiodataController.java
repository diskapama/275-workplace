package com.xa.medic.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.lowagie.text.DocumentException;
import com.xa.medic.exporter.M_BiodataExporter;
import com.xa.medic.models.M_Biodata;
import com.xa.medic.models.User;
import com.xa.medic.repositories.M_BiodataRepo;
import com.xa.medic.service.M_BiodataService;

@Controller
@RequestMapping(value="/m_biodata/") // Ini Nama Folder
public class M_BiodataController {
	
	@Autowired
	private M_BiodataRepo m_biodatarepo;
	
	@Autowired M_BiodataService service;
	
	@GetMapping(value="index")
	public ModelAndView m_biodata() {
		ModelAndView view = new ModelAndView("/m_biodata/index"); // Nama Folder/hmtl
		List<M_Biodata> m_biodataData = this.m_biodatarepo.findAll();
		view.addObject("mbdata", m_biodataData);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/m_biodata/form");
		M_Biodata m_biodata = new M_Biodata();
		view.addObject("biodataform", m_biodata);
		return view;
	}
	
	private static String UPLOADED_FOLDER = "D:\\Foto\\javafile\\";
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute M_Biodata m_biodata, 
			BindingResult result,
			@RequestParam("photofile") MultipartFile file) throws Exception {
		if(!result.hasErrors()) {
			if(file.getOriginalFilename() != "") {
				byte[] bytes = file.getBytes();
				Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
				Files.write(path, bytes);
			}
			this.m_biodatarepo.save(m_biodata);
			
		}
		return new ModelAndView("redirect:/m_biodata/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/m_biodata/form");
		M_Biodata m_biodata = this.m_biodatarepo.findById(id).orElse(null);
		view.addObject("biodataform", m_biodata);
		return view;
	}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView delbiodata(@PathVariable("id") Long id) {
		if(id != null) {
			this.m_biodatarepo.deleteById(id);
		}
		return new ModelAndView("redirect:/m_biodata/index");
	}
	
	@GetMapping(value="exporttopdf")
	public void exporttopdf(HttpServletResponse response) throws DocumentException, IOException {
		response.setContentType("application/pdf");
		DateFormat dateformat = new SimpleDateFormat("yyyymmddHHmmss");
		String currentdate = dateformat.format(new Date());
		
		String headerkey = "Content-Disposition";
		String headervalue = "attachment; filename=profile_"+currentdate+".pdf";
		response.setHeader(headerkey, headervalue);
		
		List<M_Biodata> listm_biodata = service.listAll();
		
		M_BiodataExporter exporter = new M_BiodataExporter(listm_biodata);
		exporter.export(response);
	}
}

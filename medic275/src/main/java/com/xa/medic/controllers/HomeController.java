package com.xa.medic.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller //cara memanggil controller/
@RequestMapping(value="/") // root
public class HomeController {

	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/index");
		return view;
	}
	
	@GetMapping(value="home")
	public ModelAndView home(HttpSession sess) {
		ModelAndView view = new ModelAndView("/home");
		view.addObject("sessemail", sess.getAttribute("email")); //fungsi session untuk akses login, ketika session habis maka harus login ulang
		return view;
	}
}

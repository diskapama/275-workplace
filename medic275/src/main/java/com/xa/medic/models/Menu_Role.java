package com.xa.medic.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name="menu_role")
public class Menu_Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonNull
	@Column(name="id")
	private Long Id;
	
	@Column(name="menu_id")
	private Long Menu_Id;
	
	@Column(name="role_id")
	private Long Role_Id;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getMenu_Id() {
		return Menu_Id;
	}

	public void setMenu_Id(Long menu_Id) {
		Menu_Id = menu_Id;
	}

	public Long getRole_Id() {
		return Role_Id;
	}

	public void setRole_Id(Long role_Id) {
		Role_Id = role_Id;
	}
	
	
}

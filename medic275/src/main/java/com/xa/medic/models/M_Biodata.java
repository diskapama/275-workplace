package com.xa.medic.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.lang.NonNull;
import groovyjarjarantlr4.v4.runtime.misc.Nullable;

@Entity
@Table(name="biodata")
public class M_Biodata {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonNull //Tidak Boleh Null
	@Column(name="id")
	private Long Id;
	
	@Nullable //artinya boleh null, boleh juga tidak usah pake Nullable
	@Column(name="fullname", length=255)
	private String Fullname;
	
	@Column(name="mobile_phone", length=15)
	private String Mobile_Phone;
	
	@Column(name="image", length=200)
	private String Image;
	
	@Column(name="image_path", length=255)
	private String Image_Path;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getFullname() {
		return Fullname;
	}

	public void setFullname(String fullname) {
		Fullname = fullname;
	}

	public String getMobile_Phone() {
		return Mobile_Phone;
	}

	public void setMobile_Phone(String mobile_Phone) {
		Mobile_Phone = mobile_Phone;
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public String getImage_Path() {
		return Image_Path;
	}

	public void setImage_Path(String image_Path) {
		Image_Path = image_Path;
	}
	
	
}

package com.xa.medic.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;
import groovyjarjarantlr4.v4.runtime.misc.NotNull;

@Entity
@Table(name="menu")
public class Menu {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonNull
	@Column(name="id")
	private Long Id;
	
	@Column(name="name", length=20)
	private String Name;
	
	@Column(name="url", length=50)
	private String Url;
	
	@Column(name="parent_id")
	private Long Parent_Id;
	
	@Column(name="big_icon", length=100)
	private String Big_Icon;
	
	@Column(name="small_icon", length=100)
	private String Small_Icon;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

	public Long getParent_Id() {
		return Parent_Id;
	}

	public void setParent_Id(Long parent_Id) {
		Parent_Id = parent_Id;
	}

	public String getBig_Icon() {
		return Big_Icon;
	}

	public void setBig_Icon(String big_Icon) {
		Big_Icon = big_Icon;
	}

	public String getSmall_Icon() {
		return Small_Icon;
	}

	public void setSmall_Icon(String small_Icon) {
		Small_Icon = small_Icon;
	}
	
	
}

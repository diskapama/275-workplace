package com.xa.medic.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import groovyjarjarantlr4.v4.runtime.misc.NotNull;

@Entity
@Table(name="address")
public class M_Biodata_Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="biodata_id", insertable=false, updatable=false)
	public M_Biodata joinBiodata;
	
	@Column(name="biodata_id")
	private Long Biodata_Id;
	
	@Column(name="label", length=100)
	private String Label;
	
	@Column(name="recipient", length=100)
	private String Recipient;
	
	@Column(name="recipient_phone_number", length=15)
	private String Recipient_Phone_Number;
	
	@Column(name="location_id")
	private Long Location_Id;
	
	@Column(name="postal_code", length=10)
	private String Postal_Code;
	
	@Column(name="address")
	private String Address;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBiodata_Id() {
		return Biodata_Id;
	}

	public void setBiodata_Id(Long biodata_Id) {
		Biodata_Id = biodata_Id;
	}

	public String getLabel() {
		return Label;
	}

	public void setLabel(String label) {
		Label = label;
	}

	public String getRecipient() {
		return Recipient;
	}

	public void setRecipient(String recipient) {
		Recipient = recipient;
	}

	public String getRecipient_Phone_Number() {
		return Recipient_Phone_Number;
	}

	public void setRecipient_Phone_Number(String recipient_Phone_Number) {
		Recipient_Phone_Number = recipient_Phone_Number;
	}

	public Long getLocation_Id() {
		return Location_Id;
	}

	public void setLocation_Id(Long location_Id) {
		Location_Id = location_Id;
	}

	public String getPostal_Code() {
		return Postal_Code;
	}

	public void setPostal_Code(String postal_Code) {
		Postal_Code = postal_Code;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}
	
	
	
}

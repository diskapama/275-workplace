package com.xa.medic.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name="users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NonNull
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="biodata_id", insertable=false, updatable=false)
	public M_Biodata joinBiodata;
	
	@Column(name="biodata_id")
	private Long Biodata_Id;
	
	@ManyToOne
	@JoinColumn(name="role_id", insertable=false, updatable=false)
	public Role joinRole;
	
	@Column(name="role_id")
	private Long Role_Id;
	
	@Column(name="email", length=100)
	private String Email;
	
	@Column(name="password", length=255)
	private String Password;
	
	@Column(name="login_attempt")
	private Integer Login_Attempt;
	
	@Column(name="is_locked")
	private Boolean Is_Locked;
	
	@Column(name="last_login")
	private java.sql.Date Last_Login;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getBiodata_Id() {
		return Biodata_Id;
	}

	public void setBiodata_Id(Long biodata_Id) {
		Biodata_Id = biodata_Id;
	}

	public Long getRole_Id() {
		return Role_Id;
	}

	public void setRole_Id(Long role_Id) {
		Role_Id = role_Id;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public Integer getLogin_Attempt() {
		return Login_Attempt;
	}

	public void setLogin_Attempt(Integer login_Attempt) {
		Login_Attempt = login_Attempt;
	}

	public Boolean getIs_Locked() {
		return Is_Locked;
	}

	public void setIs_Locked(Boolean is_Locked) {
		Is_Locked = is_Locked;
	}

	public Date getLast_Login() {
		return Last_Login;
	}

	public void setLast_Login(java.sql.Date last_Login) {
		Last_Login = last_Login;
	}
	
	
	
}

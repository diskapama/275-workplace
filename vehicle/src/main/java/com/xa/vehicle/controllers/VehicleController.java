package com.xa.vehicle.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xa.vehicle.models.Vehicle;
import com.xa.vehicle.repositories.VehicleRepo;

@Controller
@RequestMapping(value = "/vehicle/")
public class VehicleController {

	@Autowired
	private VehicleRepo vehiclerepo;;

	@GetMapping(value = "index")
	public ModelAndView vehicle() {
		ModelAndView view = new ModelAndView("/vehicle/index");
		List<Vehicle> vehicleData = this.vehiclerepo.findAll();
		view.addObject("vdata", vehicleData);
		return view;
	}

	@GetMapping(value = "form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/vehicle/form");
		Vehicle vehicle = new Vehicle();
		view.addObject("vehicleform", vehicle);
		List<Vehicle> vehicleList = this.vehiclerepo.findAll();
		view.addObject("combovehicle", vehicleList);
		return view;
	}

	@PostMapping(value = "save")
	public ModelAndView save(@ModelAttribute Vehicle vehicle, BindingResult result) {
		if (!result.hasErrors()) {
			this.vehiclerepo.save(vehicle);
		}
		return new ModelAndView("redirect:/vehicle/index");
	}
	
	@GetMapping(value="/edit/{id}")
	public ModelAndView editform(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("/vehicle/form");
		Vehicle vehicle = this.vehiclerepo.findById(id).orElse(null);
		view.addObject("vehicleform", vehicle);
		List<Vehicle> vehicleList = this.vehiclerepo.findAll();
		view.addObject("combovehicle", vehicleList);
		return view;
	}
	
	@GetMapping(value="/delete/{id}")
	public ModelAndView delvehicle(@PathVariable("id") Long id) {
		if(id != null) {
			this.vehiclerepo.deleteById(id);
		}
		return new ModelAndView("redirect:/vehicle/index");
	}
}

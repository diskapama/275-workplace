package com.xa.vehicle.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.vehicle.models.Vehicle;

@Repository
public interface VehicleRepo extends JpaRepository <Vehicle, Long> {

}
